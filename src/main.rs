use clap::Parser;
use crossterm::style::Color;
use rsmatrix::Matrix;

/// Show text flowing in the terminal, as seen in 'The Matrix' movie.
///
/// To abort the program, press 'Q' or 'Ctrl + C'.
///
/// The program can be paused by pressing 'Space'. In this state, you can :
///
/// - Resume the program by pressing 'Space' again. \
///
/// - Advance the program by one frame, by pressing 'N'.
#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    /// Force characters to be all ascii.
    #[clap(long)]
    ascii: bool,
    /// Use more plain-looking effects.
    ///
    /// This will be less CPU-intensive.
    #[clap(long)]
    plain: bool,
    /// First color used. Character of this color may be in bold font.
    ///
    /// Accepted values are:
    ///
    /// - ansi colors, in lowercase. For example, 'red', 'black', or 'darkblue'.
    ///
    /// - Ansi color sequences. For example :
    ///
    ///     * '5;0' means 'black'.
    ///
    ///     * '5;26' means ansi color 26.
    ///
    ///     * '2;50;60;70' means red=50, green=60 and blue=70.
    #[clap(long, value_name = "COLOR", default_value = "2;25;255;64")]
    color1: String,
    /// Second color used.
    ///
    /// This is only used when `--plain` is used.
    ///
    /// See 'color1' for more details.
    #[clap(long, value_name = "COLOR", default_value = "darkgreen")]
    color2: String,
    /// Overrides the background color.
    ///
    /// This matters when the `--plain` option is _not_ used. In this case,
    /// characters will fade off the screen: this is the last color such a
    /// character will be.
    #[clap(long, value_name = "COLOR", default_value = "black")]
    background_color: String,
}

/// Parse the given string as a [`Color`].
fn parse_color(color: &str) -> Result<Color, String> {
    Ok(match color {
        "white" => Color::White,
        "grey" => Color::Grey,
        "darkgrey" => Color::DarkGrey,
        "black" => Color::Black,
        "blue" => Color::Blue,
        "darkblue" => Color::DarkBlue,
        "cyan" => Color::Cyan,
        "darkcyan" => Color::DarkCyan,
        "green" => Color::Green,
        "darkgreen" => Color::DarkGreen,
        "red" => Color::Red,
        "darkred" => Color::DarkRed,
        "yellow" => Color::Yellow,
        "darkyellow" => Color::DarkYellow,
        "magenta" => Color::Magenta,
        "darkmagenta" => Color::DarkMagenta,
        _ => Color::parse_ansi(color).ok_or(format!("unknown color: {}", color))?,
    })
}

/// Generic error for high-level error reporting in [`main`].
struct MyError {
    error: Box<dyn std::fmt::Display>,
}

impl std::fmt::Debug for MyError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.error, f)
    }
}

impl<E: std::fmt::Display + 'static> From<E> for MyError {
    fn from(error: E) -> Self {
        Self {
            error: Box::new(error),
        }
    }
}

fn main() -> Result<(), MyError> {
    let args = Args::parse();
    let stdout = std::io::stdout();

    let color1 = parse_color(&args.color1)?;
    let color2 = parse_color(&args.color2)?;
    let background_color = parse_color(&args.background_color)?;

    let mut matrix = Matrix::new(
        stdout,
        args.ascii,
        args.plain,
        color1,
        color2,
        background_color,
    )?;
    while matrix.process()? {}
    Ok(())
}
