#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl Color {
    pub(crate) const WHITE: Self = Self {
        r: 255,
        g: 255,
        b: 255,
    };

    pub(crate) fn from_crossterm(color: crossterm::style::Color) -> Self {
        match color {
            crossterm::style::Color::Reset | crossterm::style::Color::AnsiValue(_) => {
                unreachable!()
            }
            crossterm::style::Color::Black => Self { r: 0, g: 0, b: 0 },
            crossterm::style::Color::DarkGrey => Self {
                r: 169,
                g: 169,
                b: 169,
            },
            crossterm::style::Color::Red => Self { r: 255, g: 0, b: 0 },
            crossterm::style::Color::DarkRed => Self { r: 139, g: 0, b: 0 },
            crossterm::style::Color::Green => Self { r: 0, g: 255, b: 0 },
            crossterm::style::Color::DarkGreen => Self { r: 0, g: 100, b: 0 },
            crossterm::style::Color::Yellow => Self {
                r: 255,
                g: 255,
                b: 0,
            },
            crossterm::style::Color::DarkYellow => Self {
                r: 139,
                g: 139,
                b: 0,
            },
            crossterm::style::Color::Blue => Self { r: 0, g: 0, b: 255 },
            crossterm::style::Color::DarkBlue => Self { r: 0, g: 0, b: 139 },
            crossterm::style::Color::Magenta => Self {
                r: 255,
                g: 0,
                b: 255,
            },
            crossterm::style::Color::DarkMagenta => Self {
                r: 139,
                g: 0,
                b: 139,
            },
            crossterm::style::Color::Cyan => Self {
                r: 0,
                g: 255,
                b: 255,
            },
            crossterm::style::Color::DarkCyan => Self {
                r: 0,
                g: 139,
                b: 139,
            },
            crossterm::style::Color::White => Self {
                r: 255,
                g: 255,
                b: 255,
            },
            crossterm::style::Color::Grey => Self {
                r: 128,
                g: 128,
                b: 128,
            },
            crossterm::style::Color::Rgb { r, g, b } => Self { r, g, b },
        }
    }

    pub(crate) fn mix(self, color2: Self, part: f32) -> Self {
        Self {
            r: (self.r as f32 * part + color2.r as f32 * (1.0 - part)) as u8,
            g: (self.g as f32 * part + color2.g as f32 * (1.0 - part)) as u8,
            b: (self.b as f32 * part + color2.b as f32 * (1.0 - part)) as u8,
        }
    }
}

impl From<Color> for crossterm::style::Color {
    fn from(color: Color) -> Self {
        Self::Rgb {
            r: color.r,
            g: color.g,
            b: color.b,
        }
    }
}
