use crate::{random::Random, Color};
use crossterm::{cursor, style, terminal, QueueableCommand as _};
use std::io::{Stdout, Write};

#[derive(Clone, Copy, Debug)]
pub(crate) struct ScreenChar {
    /// When this reaches `1.0`, the character should be deleted.
    pub(crate) decay: f32,
    pub(crate) speed: f32,
    pub(crate) decay_speed: f32,
    /// Character at this position.
    pub(crate) c: char,
}

/// Representation of the screen, on which we will be writing.
#[derive(Debug)]
pub(crate) struct Screen {
    ascii: bool,
    color1: Color,
    color2: Color,
    background_color: Color,
    stdout: Stdout,
    columns: u16,
    rows: u16,
    /// Column-first
    buffer: Vec<Option<ScreenChar>>,
}

impl Screen {
    /// Create a new `Screen` instance.
    ///
    /// This will switch the terminal to 'raw' mode, leaving it on drop.
    ///
    /// Only one instance of `Screen` should be created at a given time.
    pub(super) fn new(
        mut stdout: Stdout,
        ascii: bool,
        color1: Color,
        color2: Color,
        background_color: Color,
    ) -> crossterm::Result<Self> {
        stdout
            .queue(terminal::EnterAlternateScreen)?
            .queue(terminal::DisableLineWrap)?
            .queue(terminal::SetTitle("THE MATRIX"))?
            .queue(cursor::Hide)?
            .queue(style::SetBackgroundColor(background_color.into()))?;
        stdout.flush()?;
        terminal::enable_raw_mode()?;
        let (columns, rows) = terminal::size()?;
        for row in 0..rows {
            for column in 0..columns {
                stdout
                    .queue(cursor::MoveTo(column, row))?
                    .queue(style::Print(' '))?;
            }
        }
        stdout.flush()?;
        let buffer = vec![None; columns as usize * rows as usize];
        Ok(Self {
            ascii,
            color1,
            color2,
            background_color,
            stdout,
            columns,
            rows,
            buffer,
        })
    }

    /// Get the number of columns on the screen.
    pub(super) const fn columns(&self) -> u16 {
        self.columns
    }

    /// Get the number of rows on the screen.
    pub(super) const fn rows(&self) -> u16 {
        self.rows
    }

    /// Check the size of the actual terminal screen, and updates `self` if
    /// needed.
    ///
    /// If the size changed, this will also clear the screen.
    ///
    /// # Return
    /// - `Ok(true)` if the size was updated.
    /// - `Ok(false)` if the size was _not_ updated.
    /// - `Err(crossterm::Error)` if either fetching the terminal size or
    /// clearing the terminal failed.
    pub(crate) fn update_size(&mut self) -> crossterm::Result<bool> {
        let (columns, rows) = terminal::size()?;
        if (columns, rows) != (self.columns, self.rows) {
            self.columns = columns;
            self.rows = rows;
            self.stdout
                .queue(terminal::Clear(terminal::ClearType::All))?;
            self.stdout.flush()?;
            self.buffer = vec![None; columns as usize * rows as usize];
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub(crate) fn new_leading_char(&mut self, screen_char: ScreenChar, row: u16, column: u16) {
        let screen_index = column as usize * self.rows as usize + row as usize;
        self.buffer[screen_index] = Some(screen_char);
    }

    pub(crate) fn update_and_write(
        &mut self,
        random: &mut Random,
        plain: bool,
    ) -> crossterm::Result<()> {
        for column in 0..self.columns {
            for row in (0..self.rows).rev() {
                let screen_index = column as usize * self.rows as usize + row as usize;
                let screen_char = if let Some(c) = self.buffer[screen_index] {
                    c
                } else {
                    continue;
                };
                if screen_char.speed == 0.0 {
                    if plain && screen_char.decay == 0.0 {
                        self.write(
                            column,
                            row,
                            if random.get_bool() {
                                self.color1
                            } else {
                                self.color2
                            },
                            screen_char.c,
                        )?;
                    }
                    if screen_char.decay >= 1.0 {
                        self.erase(column, row)?;
                        self.buffer[screen_index] = None;
                        continue;
                    } else if !plain {
                        self.write(
                            column,
                            row,
                            self.background_color.mix(self.color1, screen_char.decay),
                            screen_char.c,
                        )?
                    }
                    if let Some(screen_char) = &mut self.buffer[screen_index] {
                        screen_char.decay += screen_char.decay_speed;
                    }
                } else {
                    self.write(column, row, Color::WHITE, screen_char.c)?;
                    if let Some(screen_char) = &mut self.buffer[screen_index] {
                        screen_char.decay += screen_char.speed;
                        if screen_char.decay >= 1.0 {
                            let mut new_screen_char = *screen_char;
                            screen_char.decay = 0.0;
                            screen_char.speed = 0.0;
                            if row + 1 < self.rows {
                                new_screen_char.decay -= 1.0;
                                new_screen_char.c = if self.ascii {
                                    random.get_ascii_char()
                                } else {
                                    random.get_utf8_char()
                                };
                                self.buffer[screen_index + 1] = Some(new_screen_char);
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    /// Write `c` at the given `column, row` position, with `color`.
    ///
    /// Writing to the terminal may raise a `crossterm::Error`.
    pub(crate) fn write(
        &mut self,
        column: u16,
        row: u16,
        color: Color,
        c: char,
    ) -> crossterm::Result<()> {
        self.stdout
            .queue(style::SetForegroundColor(color.into()))?
            .queue(cursor::MoveTo(column, row))?
            .queue(style::Print(c))?;
        Ok(())
    }

    /// Erase any character at the given `column, row` position.
    ///
    /// This may raise a `crossterm::Error`.
    pub(crate) fn erase(&mut self, column: u16, row: u16) -> crossterm::Result<()> {
        self.stdout
            .queue(cursor::MoveTo(column, row))?
            .queue(style::Print(' '))?;
        Ok(())
    }

    /// Flush [`Self::stdout`].
    pub(crate) fn flush(&mut self) -> crossterm::Result<()> {
        self.stdout.flush()
    }
}

impl Drop for Screen {
    fn drop(&mut self) {
        terminal::disable_raw_mode().unwrap();
        self.stdout
            .queue(cursor::Show)
            .unwrap()
            .queue(terminal::EnableLineWrap)
            .unwrap()
            .queue(terminal::LeaveAlternateScreen)
            .unwrap();
        self.stdout.flush().unwrap();
    }
}
