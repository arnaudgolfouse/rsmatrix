//! Module implementing a **very** simple random number generator.

static UTF8_CHARS: &[char] = &[
    'ｲ', 'ｦ', 'ｸ', 'ｺ', 'ｿ', 'ﾁ', 'ﾄ', 'ﾉ', 'ﾌ', 'ﾔ', 'ﾖ', 'ﾙ', 'ﾚ', 'ﾛ', 'ﾝ', ':', '.', '"', '_',
    '=', '*', '+', '-', '<', '>', '¦', '|', '0', '1', '2', '3', '4', '5', '7', '8', '9', 'ç',
    '╌',
    // '日', // messes up terminal output, because it is wider than the other characters.
];

static CHARS: &[u8] = br##"!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|,}~"##;

#[derive(Debug)]
pub struct Random {
    inner: u64,
}

impl Random {
    pub fn new() -> Self {
        let mut res = Self {
            inner: u64::from(
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .subsec_micros(),
            ),
        };
        res.get_u64();
        res
    }

    /// Rough random function.
    pub fn get_u64(&mut self) -> u64 {
        let rotation = (self.inner & 0b1111) as u32;
        self.inner = self
            .inner
            .rotate_left(rotation)
            .wrapping_mul(12_326_329)
            .wrapping_add(864_224_783);
        self.inner
    }

    /// Get a random float in range `start..end`.
    pub fn get_f32_range(&mut self, start: f32, end: f32) -> f32 {
        let res = (self.get_u64() & 0xfffff) as f32 / 0xfffff as f32;
        res.mul_add(end - start, start)
    }

    /// Get a uniformly distributed random u16 number.
    pub fn get_u16(&mut self) -> u16 {
        let rand = self.get_u64();
        (rand & 0xffff) as u16
    }

    /// Get a uniformly distributed character in [`UTF8_CHARS`].
    pub fn get_utf8_char(&mut self) -> char {
        UTF8_CHARS[self.get_u64() as usize % UTF8_CHARS.len()]
    }

    /// Get a uniformly distributed character in [`CHARS`].
    pub fn get_ascii_char(&mut self) -> char {
        CHARS[self.get_u64() as usize % CHARS.len()] as char
    }

    /// Get a uniformly distributed random boolean.
    pub fn get_bool(&mut self) -> bool {
        self.get_u64() % 2 == 0
    }
}
