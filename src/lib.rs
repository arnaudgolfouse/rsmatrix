mod color;
mod random;
mod screen;

use self::{color::Color, random::Random, screen::Screen};
use crossterm::event::Event;
use screen::ScreenChar;
use std::io::Stdout;

/// Main structure of the program.
#[derive(Debug)]
pub struct Matrix {
    /// `true` if we should only use ascii characters.
    ascii: bool,
    /// Use a more plain style, but potentially save ressources.
    plain: bool,
    /// The random number generator.
    random: Random,
    /// Representation of the screen we are drawing on.
    screen: Screen,
    /// `true` if we should run [`Self::process`] exactly one, and then pause
    /// again.
    one_frame: bool,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum ControlFlow {
    Continue,
    /// Continue for one frame, then pause
    ContinueOneFrame,
    Stop,
    Pause,
}

impl Matrix {
    pub fn new(
        stdout: Stdout,
        ascii: bool,
        plain: bool,
        color1: crossterm::style::Color,
        color2: crossterm::style::Color,
        background_color: crossterm::style::Color,
    ) -> crossterm::Result<Self> {
        let color1 = Color::from_crossterm(color1);
        let color2 = Color::from_crossterm(color2);
        let background_color = Color::from_crossterm(background_color);
        let random = Random::new();
        let screen = Screen::new(stdout, ascii, color1, color2, background_color)?;

        Ok(Self {
            ascii,
            plain,
            random,
            screen,
            one_frame: false,
        })
    }

    /// Run an iteration of the program, updating the screen.
    ///
    /// Note that this will block the current thread for at least 32ms, or more
    /// if  
    pub fn process(&mut self) -> crossterm::Result<bool> {
        self.screen.update_size()?;
        let to_spawn = self
            .random
            .get_f32_range(0.0, self.screen.columns() as f32 / 60.0) as u8;
        for _ in 0..to_spawn {
            let mut row = self.random.get_u16() % self.screen.rows();
            let rand = self.random.get_f32_range(0.0, 1.0);
            if row > self.screen.rows() / 4 {
                if rand <= 0.6 {
                    row %= self.screen.rows() / 4;
                } else if rand <= 0.8 {
                    row = 0;
                }
            }

            let column = self.random.get_u16() % self.screen.columns();
            let screen_char = ScreenChar {
                decay: 0.0,
                speed: self.random.get_f32_range(0.5, 1.5),
                decay_speed: self.random.get_f32_range(0.005, 0.08),
                c: if self.ascii {
                    self.random.get_ascii_char()
                } else {
                    self.random.get_utf8_char()
                },
            };
            self.screen.new_leading_char(screen_char, row, column);
        }
        self.screen.update_and_write(&mut self.random, self.plain)?;
        self.screen.flush()?;

        let mut paused = self.one_frame;
        self.one_frame = false;
        loop {
            match Self::poll_event()? {
                ControlFlow::Continue => {
                    if paused {
                        std::thread::sleep(std::time::Duration::from_millis(50));
                    } else {
                        break;
                    }
                }
                ControlFlow::ContinueOneFrame => {
                    if paused {
                        self.one_frame = true;
                    }
                    break;
                }
                ControlFlow::Stop => return Ok(false),
                ControlFlow::Pause => {
                    paused = !paused;
                }
            }
        }

        Ok(true)
    }

    /// Get a user event, like keystrokes.
    ///
    /// This will block for 32ms, and then return [`ControlFlow::Continue`] if no
    /// event happened.
    ///
    /// # Return
    /// - If a user presses 'Q' or 'Ctrl-C', we need to quit.
    /// - If a user presses space, we pause.
    /// - Else, we continue as usual.
    fn poll_event() -> crossterm::Result<ControlFlow> {
        use crossterm::event::{self, KeyCode, KeyModifiers};

        if event::poll(std::time::Duration::from_millis(32))? {
            let event = event::read()?;
            if let Event::Key(key) = event {
                if (key.modifiers.contains(KeyModifiers::CONTROL) && key.code == KeyCode::Char('c'))
                    || key.code == KeyCode::Char('q')
                    || key.code == KeyCode::Char('Q')
                {
                    return Ok(ControlFlow::Stop);
                } else if key.code == KeyCode::Char(' ') {
                    return Ok(ControlFlow::Pause);
                } else if key.code == KeyCode::Char('n') || key.code == KeyCode::Char('N') {
                    return Ok(ControlFlow::ContinueOneFrame);
                }
            }
        }
        Ok(ControlFlow::Continue)
    }
}
